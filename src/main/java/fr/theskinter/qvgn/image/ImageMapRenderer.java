package fr.theskinter.qvgn.image;

import java.awt.image.BufferedImage;

import org.bukkit.entity.Player;
import org.bukkit.map.MapCanvas;
import org.bukkit.map.MapRenderer;
import org.bukkit.map.MapView;

import lombok.Setter;

public class ImageMapRenderer extends MapRenderer {

	@Setter private BufferedImage image;
	@Setter private boolean shouldRender;
	
	
	public ImageMapRenderer(BufferedImage image) {
		this.image = image;
		this.shouldRender = true;
	}
	
	@Override
	public void render(MapView mapView, MapCanvas mapCanvas, Player player) {
		if (shouldRender) {
			mapCanvas.drawImage(0, 0, image);
			setShouldRender(false);
		}
	}

}
