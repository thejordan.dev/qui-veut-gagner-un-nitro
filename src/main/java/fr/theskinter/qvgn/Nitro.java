package fr.theskinter.qvgn;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import fr.theskinter.qvgn.core.SchedulerManager;
import fr.theskinter.qvgn.managers.Commands;
import fr.theskinter.qvgn.managers.Events;
import fr.theskinter.qvgn.managers.Menus;
import fr.theskinter.qvgn.managers.UpdatingMenu;
import lombok.Getter;

public class Nitro extends JavaPlugin implements Listener {

	public static Nitro instance;
	
	@Getter private File mapsIdsFile = new File(getDataFolder(),"maps.yml");
	@Getter private File screenFile = new File(getDataFolder(),"screen.yml");
	@Getter private File sceneFile = new File(getDataFolder(),"scene.yml");
	@Getter private File imagesFolder = new File(getDataFolder(),"images");
	
	@Override
	public void onEnable() {
		instance = this;
		setupFiles();
		Menus.init();
		Events.init(this);
		Commands.init(this);
		Game.init();
		SchedulerManager.init();
		getServer().getPluginManager().registerEvents(this, this);
		getConfig().set("ids", new ArrayList<>());
		saveConfig();
	}

	private void setupFiles() {
		if (!mapsIdsFile.exists()) {
			try { mapsIdsFile.getParentFile().mkdirs(); mapsIdsFile.createNewFile(); }
			catch (IOException e) { e.printStackTrace(); }
		}
		if (!screenFile.exists()) {
			try { screenFile.getParentFile().mkdirs(); screenFile.createNewFile(); }
			catch (IOException e) { e.printStackTrace(); }
		}
		if (!imagesFolder.exists()) { imagesFolder.mkdirs(); }
		for (int i = 1;i<=15;i++) {
			File questionFolder = new File(getDataFolder(),"images/question"+i);
			if (!questionFolder.exists()) {
				questionFolder.mkdirs();
			}
		}
	}

	@Override
	public void onDisable() {
		for (UpdatingMenu menu : Menus.instance.getUpdatables()) {
			Bukkit.getScheduler().cancelTask(menu.getTaskID());
		}
		for (NamespacedKey keys : Game.instance.getBossBars()) {
			Bukkit.getBossBar(keys).removeAll();
			Bukkit.removeBossBar(keys);
		}
		Game.instance.save();
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		if (event.getSlotType() == SlotType.OUTSIDE) return;
		if (event.getClickedInventory().getType() != InventoryType.CHEST) return;
		if (event.getInventory().getHolder() == null) return;
		if (Menus.instance.doesMenuExist(event.getClickedInventory().getHolder())) {
			Player who = (Player)event.getWhoClicked(); ItemStack item = event.getCurrentItem();
			ClickType click = event.getClick(); boolean shift = event.isShiftClick();
			boolean left = event.isLeftClick(); boolean right = event.isRightClick();
			Menus.instance.getMenu(event.getInventory().getHolder()).onClick(who, item, click, shift, left, right,event);
		}
	}
	
	@EventHandler
	public void onInventoryClose(InventoryCloseEvent event) {
		if (Menus.instance.doesMenuExist(event.getInventory().getHolder())) {
			Menus.instance.getMenu(event.getInventory().getHolder()).onClose((Player)event.getPlayer(), event);
		}
	}
	
	
}
