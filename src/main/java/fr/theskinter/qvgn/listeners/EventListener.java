package fr.theskinter.qvgn.listeners;

import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import fr.theskinter.qvgn.managers.Events;

public class EventListener implements Listener {

	public EventListener(JavaPlugin plugin) {
		Events.instance.registerEvents(this);
	}
	
}
