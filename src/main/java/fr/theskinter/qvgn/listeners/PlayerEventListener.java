package fr.theskinter.qvgn.listeners;

import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

import fr.theskinter.qvgn.Game;
import fr.theskinter.qvgn.Game.RegisterType;
import fr.theskinter.qvgn.objects.Seat;


public class PlayerEventListener extends EventListener {

	public PlayerEventListener(JavaPlugin plugin) {
		super(plugin);
	}
	
	@EventHandler
	public void PlayerInteractItemFrameEvent(PlayerInteractEntityEvent event) {
		Player player = event.getPlayer();
		if (event.getHand() != EquipmentSlot.OFF_HAND) return;
		if (!(event.getRightClicked() instanceof ItemFrame)) return;
		if (!Game.instance.getPlayersStates().containsKey(player.getUniqueId())) return;
		if (Game.instance.getPlayersStates().get(player.getUniqueId()) == RegisterType.SEAT) return;
		RegisterType regType = Game.instance.getPlayersStates().get(player.getUniqueId());
		ItemFrame frame = (ItemFrame) event.getRightClicked();
		BlockFace face = frame.getAttachedFace();
		Block block = frame.getLocation().getBlock();
		if (regType == RegisterType.bTOP_LEFT) {
			Game.instance.getScreen().getBorder().setTopLeftLoc(block.getLocation());
			player.sendMessage("�6�lCoin sup�rieur gauche s�lectionner !");
			player.sendMessage("�a�lVeuillez s�lectionner le coin inf�rieur droit !");
			Game.instance.getPlayersStates().put(player.getUniqueId(), RegisterType.bBOT_RIGHT);
		} else if (regType == RegisterType.bBOT_RIGHT) {
			if (Game.instance.getScreen().getBorder().getTopLeftLoc().getWorld().getUID().equals(block.getLocation().getWorld().getUID())) {
				player.sendMessage("�6Coin inf�rieur droit de l'�cran d�finie");
				Game.instance.getPlayersStates().put(player.getUniqueId(), RegisterType.NONE);
				Vector bet = Game.instance.getScreen().getBorder().getTopLeftLoc().toVector().subtract(block.getLocation().toVector());
				if (bet.getBlockX() == 0) {
					if (Math.abs(bet.getBlockZ()) == 10 && Math.abs(bet.getBlockY()) == 5) {
						if (Game.instance.getScreen().getBorder().getTopLeftLoc().getBlockY()>block.getY()) {
							player.sendMessage("�a�lEcran d�finie avec succ�s !");
							Game.instance.getScreen().getBorder().setBotRightLoc(block.getLocation());
							Game.instance.getScreen().setFacing(face);
						} else {
							player.sendMessage("�c�lERREUR : Le coin sup�rieur gauche est inf�rieur au coin inf�rieur droit.");
						}
					} else {
						player.sendMessage("�c�lERREUR : La r�solution de l'�cran dois �tre du 11 / 5 !");
					}
				} else if (bet.getBlockZ() == 0) {
					if (Math.abs(bet.getBlockX()) == 10 && Math.abs(bet.getBlockY()) == 5) {
						if (Game.instance.getScreen().getBorder().getTopLeftLoc().getBlockY()>block.getY()) {
							player.sendMessage("�a�lEcran d�finie avec succ�s !");
							Game.instance.getScreen().getBorder().setBotRightLoc(block.getLocation());
							Game.instance.getScreen().setFacing(face);
						} else {
							player.sendMessage("�c�lERREUR : Le coin sup�rieur gauche est inf�rieur au coin inf�rieur droit.");
						}
					} else {
						player.sendMessage("�c�lERREUR : La r�solution de l'�cran dois �tre du 11 / 5 !");
					}
				} else {
					player.sendMessage("�c�lERREUR : Mauvais positionnement des coins.");
				}
			} else {
				player.sendMessage("�c�lERREUR : Les coins doivent �tre dans le m�me monde.");
			}
		}
	}
	
	@EventHandler
	public void onBlockInteract(PlayerInteractEvent event) {
		if (event.getAction() != Action.RIGHT_CLICK_BLOCK) return;
		if (event.getHand() != EquipmentSlot.HAND) return;
		System.out.println("interact");
		Block block = event.getClickedBlock();
		if (!Game.instance.getPlayersStates().containsKey(event.getPlayer().getUniqueId())) return;
		if (Game.instance.getPlayersStates().get(event.getPlayer().getUniqueId()) != RegisterType.SEAT) return;
		if (Seat.canBeSeat(block)) {
			if (!Game.instance.isSeatRegistered(block)) {
				Game.instance.registerSeat(block);
				event.getPlayer().sendMessage("�a�lSi�ge enregistrer !");	
			} else {
				Game.instance.unregisterSeat(block);
				event.getPlayer().sendMessage("�c�lSi�ge supprimer !");
			}
		}
	}

}
