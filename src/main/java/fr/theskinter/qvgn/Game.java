package fr.theskinter.qvgn;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.ArmorStand;

import fr.theskinter.qvgn.core.SpecsSkins;
import fr.theskinter.qvgn.core.Utils;
import fr.theskinter.qvgn.objects.Screen;
import fr.theskinter.qvgn.objects.Seat;
import lombok.Getter;
import lombok.Setter;

public class Game {

	public enum RegisterType {
		NONE,bTOP_LEFT,bBOT_RIGHT,SEAT;
	}

	public static Game instance;
	
	@Getter private YamlConfiguration mapsConfiguration;
	@Getter private YamlConfiguration screenConfiguration;
	@Getter private YamlConfiguration sceneConfiguration;
	
	
	@Getter private List<Integer> mapsIds = new ArrayList<>();
	@Getter private Map<UUID,RegisterType> playersStates = new HashMap<>();
	@Getter private List<NamespacedKey> bossBars = new ArrayList<>();
	@Getter @Setter private List<SpecsSkins> usableSkins = Utils.arrayToList(SpecsSkins.values());
	@Getter @Setter private List<Seat> seats = new ArrayList<Seat>();

	@Getter private Screen screen;

	@Getter @Setter private Location sceneCenter;
	
	public Game() {
		instance = this;
		Block b1 = Bukkit.getWorlds().get(0).getBlockAt(0,0,0); b1.setType(Material.SANDSTONE_STAIRS);
		Block b2 = Bukkit.getWorlds().get(0).getBlockAt(0,1,0); b2.setType(Material.SANDSTONE_STAIRS);
		Block b3 = Bukkit.getWorlds().get(0).getBlockAt(0,2,0); b3.setType(Material.SANDSTONE_STAIRS);
		this.mapsConfiguration = YamlConfiguration.loadConfiguration(Nitro.instance.getMapsIdsFile());
		this.screenConfiguration = YamlConfiguration.loadConfiguration(Nitro.instance.getScreenFile());
		this.sceneConfiguration = YamlConfiguration.loadConfiguration(Nitro.instance.getSceneFile());
		List<Integer> loadedIds = loadMapsIds();
		if (loadedIds != null) {
			mapsIds.addAll(loadedIds);
		}
		this.screen = loadScreen();
		if (this.getScreen().getCurrentImage() != null) {
			try { this.getScreen().drawImage(ImageIO.read(this.getScreen().getCurrentImage())); }
			catch (IOException e) { e.printStackTrace(); }
		}
		this.sceneCenter = loadCenter();
		this.seats = loadSeats();
	}
	
	public int getSpecIndex() {
		int specIndex = 0;
		for (Seat seat : seats) {
			if (seat.isNPC()) {
				specIndex++;
			}
		}
		return specIndex;
	} 
	
	public Screen loadScreen() {
		Screen screen = new Screen();
		BlockFace facing = null;
		int tLx=0,tLy = 0,tLz = 0; String tLWorldID = null;
		int bRx=0,bRy=0,bRz=0; String bRWorldID = null;
		if (this.screenConfiguration.isSet("facing")) {
			facing = BlockFace.valueOf(this.screenConfiguration.getString("facing"));
			screen.setFacing(facing);
		}
		if (this.screenConfiguration.isConfigurationSection("border")) {
			ConfigurationSection border = this.screenConfiguration.getConfigurationSection("border");
			ConfigurationSection topLeftCorner = border.getConfigurationSection("topLeftCorner");
			tLx = topLeftCorner.getInt("x");
			tLy = topLeftCorner.getInt("y");
			tLz = topLeftCorner.getInt("z");
			tLWorldID = topLeftCorner.getString("world");
			ConfigurationSection bottomRightCorner = border.getConfigurationSection("botRightCorner");
			bRx = bottomRightCorner.getInt("x"); bRy = bottomRightCorner.getInt("y"); bRz = bottomRightCorner.getInt("z");
			bRWorldID = bottomRightCorner.getString("world");
			screen.getBorder().setTopLeftLoc(new Location(Bukkit.getWorld(UUID.fromString(tLWorldID)), tLx, tLy, tLz));
			screen.getBorder().setBotRightLoc(new Location(Bukkit.getWorld(UUID.fromString(bRWorldID)), bRx, bRy, bRz));
		}
		if (this.screenConfiguration.isSet("current")) {
			File current = new File(this.screenConfiguration.getString("current"));
			if (current.exists()) {
				screen.setCurrentImage(current);
			}
		}
		return screen;
	}
	
	@SuppressWarnings("unchecked")
	public List<Integer> loadMapsIds() {
		List<?> list = mapsConfiguration.getList("ids");
		if (list.isEmpty() && list.size() == 0) return null;
		if (list.size() != 66) return null;
		return (List<Integer>) list;
	}
	
	private List<Seat> loadSeats() {
		List<Seat> seatS = new ArrayList<>();
		if (this.sceneConfiguration.isConfigurationSection("seats")) {
			ConfigurationSection seatsC = this.sceneConfiguration.getConfigurationSection("seats");
			for (String key : seatsC.getKeys(false)) {
				int x,y,z; String worldID;
				ConfigurationSection seat = seatsC.getConfigurationSection(key);
				x = seat.getInt("x");
				y = seat.getInt("y");
				z = seat.getInt("z");
				worldID = seat.getString("world");
				seatS.add(new Seat(new Location(Bukkit.getWorld(UUID.fromString(worldID)), x, y, z).getBlock()));
			}
		}
		return seatS;
	}

	private Location loadCenter() {
		if (this.sceneConfiguration.isConfigurationSection("center")) {
			ConfigurationSection seatsC = this.sceneConfiguration.getConfigurationSection("center");
			int x,y,z; String worldID;
			x = seatsC.getInt("x");
			y = seatsC.getInt("y");
			z = seatsC.getInt("z");
			worldID = seatsC.getString("world");
			return new Location(Bukkit.getWorld(UUID.fromString(worldID)), x, y, z);
		}
		return null;
	}
	
	public void save() {
		saveMapsIds();
		saveScreenInfos();
		saveScene();
	}
	
	private void saveScene() {
		saveCenter();
		saveSeats();
		try { sceneConfiguration.save(Nitro.instance.getSceneFile()); }
		catch (IOException e) { e.printStackTrace(); }
	}

	private void saveSeats() {
		ConfigurationSection seats = this.sceneConfiguration.createSection("seats");
		for (int i = 0;i<getSeats().size();i++) {
			ConfigurationSection currSeat = seats.createSection(""+i);
			currSeat.set("x", getSeats().get(i).getBlock().getX());
			currSeat.set("y", getSeats().get(i).getBlock().getY());
			currSeat.set("z", getSeats().get(i).getBlock().getZ());
			currSeat.set("world", getSeats().get(i).getBlock().getWorld().getUID().toString());
		}
	}

	private void saveCenter() {
		if (getSceneCenter() != null) {
			ConfigurationSection center = this.sceneConfiguration.createSection("center");
			center.set("x", getSceneCenter().getX());
			center.set("y", getSceneCenter().getY());
			center.set("z", getSceneCenter().getZ());
			center.set("world", getSceneCenter().getWorld().getUID().toString());
		}
	}

	public void saveScreenInfos() {
		if (screen.getFacing() != null) { this.screenConfiguration.set("facing", getScreen().getFacing().name()); }
		if (screen.getBorder().getTopLeftLoc() != null && screen.getBorder().getBotRightLoc() != null) {
			ConfigurationSection border = this.screenConfiguration.createSection("border");
			ConfigurationSection topLeftCorner = border.createSection("topLeftCorner");
			topLeftCorner.set("x", screen.getBorder().getTopLeftLoc().getBlockX());
			topLeftCorner.set("y", screen.getBorder().getTopLeftLoc().getBlockY());
			topLeftCorner.set("z", screen.getBorder().getTopLeftLoc().getBlockZ());
			topLeftCorner.set("world", screen.getBorder().getTopLeftLoc().getWorld().getUID().toString());
			ConfigurationSection bottomRightCorner = border.createSection("botRightCorner");
			bottomRightCorner.set("x", screen.getBorder().getBotRightLoc().getBlockX());
			bottomRightCorner.set("y", screen.getBorder().getBotRightLoc().getBlockY());
			bottomRightCorner.set("z", screen.getBorder().getBotRightLoc().getBlockZ());
			bottomRightCorner.set("world", screen.getBorder().getBotRightLoc().getWorld().getUID().toString());
		}
		if (screen.getCurrentImage() != null && screen.getCurrentImage().exists()) {
			this.screenConfiguration.set("current", screen.getCurrentImage().getPath());
		}
		try { screenConfiguration.save(Nitro.instance.getScreenFile()); }
		catch (IOException e) { e.printStackTrace(); }
	}
	
	public void saveMapsIds() {
		mapsConfiguration.set("ids", mapsIds);
		try { mapsConfiguration.save(Nitro.instance.getMapsIdsFile()); }
		catch (IOException e) { e.printStackTrace(); }
	}
	
	public static void init() {
		if (instance != null) return;
		new Game();
	}
	
	public boolean isSeatRegistered(Block block) {
		for (Seat seat : seats) {
			if (block.getLocation().equals(seat.getBlock().getLocation())) { return true; }
		} return false;
	}
	public boolean isSeatRegistered(Location loc) { return isSeatRegistered(loc.getBlock()); }
	
	public boolean isSeatRegistered(ArmorStand stand) {
		for (Seat seat : getSeats()) {
			if (stand.getUniqueId().equals(UUID.fromString(seat.getStandID()))) {
				return true;
			}
		}
		return false;
	}
	
	public Seat getSeat(Block block) {
		for (Seat seat : seats) {
			if (block.getLocation().equals(seat.getBlock().getLocation())) { return seat; }
		} return null;
	}
	public Seat getSeat(Location location) { return getSeat(location.getBlock()); }

	public Seat getSeat(ArmorStand stand) {
		for (Seat seat : getSeats()) {
			if (stand.getUniqueId().equals(UUID.fromString(seat.getStandID()))) {
				return seat;
			}
		}
		return null;
	}
	
	public void registerSeat(Block block) {
		if (!isSeatRegistered(block)) { seats.add(new Seat(block)); }
	}
	public void registerSeat(Location location) { registerSeat(location.getBlock()); }
	
	
	public void unregisterSeat(Block block) {
		if (isSeatRegistered(block)) { getSeat(block).unRegister(); seats.remove(getSeat(block)); }
	}
	public void unregisterSeat(Location location) { unregisterSeat(location.getBlock()); }

	
}
