package fr.theskinter.qvgn.core;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import javax.imageio.ImageIO;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.map.MapRenderer;
import org.bukkit.map.MapView;

import com.google.common.io.Files;

public class Utils {

	public static List<File> getFoldersFrom(String path) {
		List<File> folders = new ArrayList<>();
		File file = new File(path);
		if (!file.exists()) return folders;
		if (!file.isDirectory()) return folders;
		for (File lFile : file.listFiles()) {
			if (lFile.isDirectory()) {
				folders.add(lFile);
			}
		}
		return folders;
	}

	public static Location getRelative(Location loc,double x,double y,double z) { return loc.clone().add(x,y,z); }
	
	public static <T> T randomFromList(List<T> usableSkins)  {
		Random random = new Random(); return usableSkins.get(random.nextInt(usableSkins.size()));
	}
	
	public static <T> List<T> arrayToList(T[] list) {
		List<T> returned = new ArrayList<T>();
		for (T ele : list) {
			returned.add(ele);
		}
		return returned;
	}
	
	public static List<File> getQuestionFolders(String path) {
		List<File> folders = new ArrayList<>();
		File file = new File(path);
		if (!file.exists()) return folders;
		if (!file.isDirectory()) return folders;
		for (File lFile : file.listFiles()) {
			if (lFile.isDirectory()) {
				if (lFile.getName().contains("question")) {
					folders.add(lFile);
				}
				continue;
			}
		}
		return folders;
	}
	
	public static List<File> getOthersFolders(String path) {
		List<File> folders = new ArrayList<>();
		File file = new File(path);
		if (!file.exists()) return folders;
		if (!file.isDirectory()) return folders;
		for (File lFile : file.listFiles()) {
			if (lFile.isDirectory()) {
				if (!lFile.getName().contains("question")) {
					folders.add(lFile);
				}
				continue;
			}
		}
		return folders;
	}
	
	public static List<File> getImagesFrom(String path) {
		List<File> files = new ArrayList<>();
		File file = new File(path);
		if (!file.exists()) return files;
		if (!file.isDirectory()) return files;
		for (File lFile : file.listFiles()) {
			if (lFile.isFile()) {
				if (Files.getFileExtension(lFile.getName()) == null) continue;
				if (!Files.getFileExtension(lFile.getName()).equals("png")) continue;
				BufferedImage image = null;
				try { if (ImageIO.read(lFile) == null) continue; image = ImageIO.read(lFile); }
				catch (IOException e) { e.printStackTrace(); }
				if (image == null) continue;
				if (image.getWidth() == 1408 && image.getHeight() == 768) {
					files.add(lFile);
				}
			}
		}
		return files;
	}
	
	public static BufferedImage fromFile(File file) {
		if (file.isFile()) {
			if (Files.getFileExtension(file.getName()) == null) return null;
			BufferedImage image = null;
			try { if (ImageIO.read(file) == null) return null; image = ImageIO.read(file); }
			catch (IOException e) { e.printStackTrace(); }
			if (image == null) return null;
			return image;
		}
		return null;
	}

	public static void fill(Location max,Location min, Material material) {
		fill(max, min, material, false);
	}
	public static void fill(Location max,Location min, Material material,boolean onlyAir) {
		if (!max.getWorld().getName().equals(min.getWorld().getName())) return;
		for(int x = (int) Math.max(max.getBlockX(), min.getBlockX()); x >= (int) Math.min(min.getBlockX(), max.getBlockX()); x--) {
		    for(int y = (int) Math.max(max.getBlockY(), min.getBlockY()); y >= (int) Math.min(min.getBlockY(), max.getBlockY()); y--) {
		        for(int z = (int) Math.max(max.getBlockZ(), min.getBlockZ()); z >= (int) Math.min(min.getBlockZ(), max.getBlockZ()); z--) {
		        	Block block = max.getWorld().getBlockAt(x, y, z);
		            if (onlyAir) {
		            	if (block.getType() == Material.AIR) { block.setType(material); }
		            } else { block.setType(material); }
		        }
		    }
		}
	}
	
	public static List<Location> listLocs(Location max,Location min) {
		List<Location> locations = new ArrayList<>();
		if (!max.getWorld().getName().equals(min.getWorld().getName())) return locations;
		for(int x = (int) Math.max(max.getBlockX(), min.getBlockX()); x >= (int) Math.min(min.getBlockX(), max.getBlockX()); x--) {
		    for(int y = (int) Math.max(max.getBlockY(), min.getBlockY()); y >= (int) Math.min(min.getBlockY(), max.getBlockY()); y--) {
		        for(int z = (int) Math.max(max.getBlockZ(), min.getBlockZ()); z >= (int) Math.min(min.getBlockZ(), max.getBlockZ()); z--) {
		        	Block block = max.getWorld().getBlockAt(x, y, z);
		            locations.add(block.getLocation());
		        }
		    }
		}
		return locations;
	}
	
	public static Location centerByFace(Location from) {
		Location loc = from.clone();
		loc.add(0.5D,0.5D,0.5D);
		return loc;
	}
	
	public static MapView resetRenderers(MapView map) {
		Iterator<MapRenderer> iterator = map.getRenderers().iterator();
		while (iterator.hasNext()) {
			map.removeRenderer(iterator.next());
		}
		return map;
	}
	
	public Optional<String> getExtensionByStringHandling(String filename) {
	    return Optional.ofNullable(filename)
	      .filter(f -> f.contains("."))
	      .map(f -> f.substring(filename.lastIndexOf(".") + 1));
	}
	
}
