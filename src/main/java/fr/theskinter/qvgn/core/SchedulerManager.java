package fr.theskinter.qvgn.core;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.NamespacedKey;
import org.bukkit.block.BlockFace;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ItemFrame;
import org.bukkit.scheduler.BukkitRunnable;

import fr.theskinter.qvgn.Game;
import fr.theskinter.qvgn.Nitro;
import fr.theskinter.qvgn.Game.RegisterType;
import lombok.Getter;

public class SchedulerManager {

	public static SchedulerManager instance;
	
	@Getter private PlayerRegistryStateScheduler playerRegistryState;
	@Getter private DelayedFrameSpawning frame;
	
	public SchedulerManager() {
		instance = this;
		this.playerRegistryState = new PlayerRegistryStateScheduler();
		this.playerRegistryState.runTaskTimer(Nitro.instance, 0L, this.playerRegistryState.loop.longValue());
	}
	
	public static void init() {
		if (instance != null) return;
		new SchedulerManager();
	}
	
	public void setupFrame(List<Location> locs,BlockFace face) {
		this.frame = new DelayedFrameSpawning(locs, face);
	}
	
	public void launchItemFrame() {
		this.frame.runTaskTimer(Nitro.instance,10L,10L);
	}
	
	public class PlayerRegistryStateScheduler extends BukkitRunnable {

		private Integer loop = 10;
		
		@Override
		public void run() {
			ArrayList<UUID> removeQueue = new ArrayList<>();
			for (UUID uuid : Game.instance.getPlayersStates().keySet()) {
				if (Bukkit.getOfflinePlayer(uuid).isOnline()) {
					BossBar bar;
					NamespacedKey bbKey = new NamespacedKey(Nitro.instance, uuid.toString()+".nitro.register.state");
					if (Bukkit.getBossBar(bbKey) == null) { bar = Bukkit.createBossBar(bbKey, "", BarColor.YELLOW, BarStyle.SOLID); }
					else { bar = Bukkit.getBossBar(bbKey); }
					if (!Game.instance.getBossBars().contains(bbKey)) {
						Game.instance.getBossBars().add(bbKey);
					}
					bar.setProgress(1); bar.addPlayer(Bukkit.getPlayer(uuid));
					RegisterType state = Game.instance.getPlayersStates().get(uuid);
					if (state == RegisterType.bTOP_LEFT) { bar.setTitle("žažlENREGISTRER LE COIN SUPPERIEUR GAUCHE !"); }
					else if (state == RegisterType.bBOT_RIGHT) { bar.setTitle("žažlENREGISTRER LE COIN INFERIEUR DROIT !"); }
					else if (state == RegisterType.SEAT) { bar.setTitle("žažlENREGISTRER DES SIEGES !"); }
					else { bar.setVisible(false); Bukkit.removeBossBar(bbKey); removeQueue.add(uuid); }
				} else {
					NamespacedKey bbKey = new NamespacedKey(Nitro.instance, uuid.toString()+".nitro.register.state");
					if (Bukkit.getBossBar(bbKey) != null) {  Bukkit.removeBossBar(bbKey); removeQueue.add(uuid); }
				}
			}
			for (UUID uuid : removeQueue) { Game.instance.getPlayersStates().remove(uuid); }
		}
		
	}
	
	public class DelayedFrameSpawning extends BukkitRunnable {

		int i = 0;
		private List<Location> locsG;
		private BlockFace facing;
		
		public DelayedFrameSpawning(List<Location> locs,BlockFace facing) {
			this.locsG = locs;
			this.facing = facing;
		}
		
		@Override
		public void run() {
			/*Collection<Entity> entities = locsG.get(i).getWorld().getNearbyEntities(locsG.get(i), 2,2,2);
			for (Entity entity : entities) {
				if (entity instanceof ItemFrame) {
					entity.remove();
				}
			}*/
			ItemFrame frame = (ItemFrame) locsG.get(i).getWorld().spawnEntity(locsG.get(i), EntityType.ITEM_FRAME);
			frame.setFacingDirection(facing);
			i = i+1;
			if (i == locsG.size()) {
				cancel();
			}
		}
		
	}
	
}
