package fr.theskinter.qvgn.objects;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.MapMeta;
import org.bukkit.map.MapView;
import org.bukkit.map.MapView.Scale;

import fr.theskinter.qvgn.Game;
import fr.theskinter.qvgn.Nitro;
import fr.theskinter.qvgn.core.Utils;
import fr.theskinter.qvgn.image.ImageMapRenderer;
import lombok.Getter;
import lombok.Setter;

public class Screen {
	
	public static final int widthP = 1408; 
	public static final int heightP = 768;
	public static final int mapRezP = 128;
	public static final int maxMapNum = 66;
	
	@Setter @Getter private BlockFace facing;
		
	@Getter private ScreenBorder border;
	
	@Getter @Setter private File currentImage;
	
	public Screen() { 
		this.border = new ScreenBorder();
	}
	
	public boolean isSetup() {
		if (getBorder().getTopLeftLoc() != null && getBorder().getBotRightLoc() != null && getFacing() != null) {
			return true;
		}
		return false;
	}

	public void drawImage(BufferedImage image) {
		getBorder().drawImageOnScreen(image, Game.instance.getMapsIds(), loopLocationsInBetween());
	}
	
	public List<Location> loopLocationsInBetween() {
		return loopLocationsInBetween(0, 0, 0);
	}
	public List<Location> loopLocationsInBetween(int modX,int modY,int modZ) {
		List<Location> locs = new ArrayList<Location>();
		if (!isSetup()) return locs;
		Location min = getBorder().getTopLeftLoc();
		Location max = getBorder().getBotRightLoc();
		if (!max.getWorld().getName().equals(min.getWorld().getName())) return locs;
		if (getFacing() == BlockFace.NORTH || getFacing() == BlockFace.SOUTH) {
		    for(int y = (int) Math.max(max.getBlockY(), min.getBlockY()); y >= (int) Math.min(min.getBlockY(), max.getBlockY()); y--) {
		    	if (getFacing() == BlockFace.NORTH) {
		    		for(int x = min.getBlockX(); x <= max.getBlockX(); x++) {
		    			Block block = max.getWorld().getBlockAt(x, y, min.getBlockZ());
		    			locs.add(block.getLocation());
			    	}
		    	} else {
		    		for(int x = min.getBlockX(); x >= max.getBlockX(); x--) {
		    			Block block = max.getWorld().getBlockAt(x, y, min.getBlockZ());
		    			locs.add(block.getLocation());
			    	}
		    	}
			}
		} else if (getFacing() == BlockFace.EAST || getFacing() == BlockFace.EAST) {
		    for(int y = (int) Math.max(max.getBlockY(), min.getBlockY()); y >= (int) Math.min(min.getBlockY(), max.getBlockY()); y--) {
		    	for(int z = (int) Math.max(max.getBlockZ(), min.getBlockZ()); z <= (int) Math.min(min.getBlockZ(), max.getBlockZ()); z--) {
			        Block block = max.getWorld().getBlockAt(min.getBlockX(), y, z);
			        locs.add(block.getLocation());
			    }
			}
		}
		return locs;

	}
	
	public class ScreenBorder {
	
		@Getter @Setter private Location topLeftLoc;
		@Getter @Setter private Location botRightLoc;
		
		public ScreenBorder() { }
		
		@SuppressWarnings("deprecation")
		public void drawImageOnScreen(BufferedImage image,List<Integer> mapsIds,List<Location> frames) {
			int rendInd = 0;
			Map<Integer,ImageMapRenderer> renderers = new HashMap<Integer,ImageMapRenderer>();
			Collections.sort(mapsIds);
			final int row = 768 / 128;
			final int cols = 1408 / 128;
			for (int i = 0; i < row; i++) {
				for (int j = 0; j < cols; j++) {
					ImageMapRenderer renderer = new ImageMapRenderer(image.getSubimage(j*128,i*128, 128, 128));
					renderers.put(mapsIds.get(rendInd), renderer);
					rendInd++;
				}
			}
			Bukkit.getScheduler().runTaskLater(Nitro.instance, new Runnable() {
				MapView map;
				int index = 0;
				@Override
				public void run() {
					for (int i = 0; i < row; i++) {
						for (int j = 0; j < cols; j++) {
							map = Bukkit.getMap(mapsIds.get(index));
							map = Utils.resetRenderers(map);
							map.setScale(Scale.FARTHEST);
							map.setUnlimitedTracking(false);
							map.addRenderer(renderers.get(mapsIds.get(index)));
							for (Player player : Bukkit.getOnlinePlayers()) {
								player.sendMap(map);
							}
							index++;
						}
					}
					for (int i = 0;i<Screen.maxMapNum;i++) {
						Integer mapId = mapsIds.get(i);
						Location frameLoc = frames.get(i);
						Collection<Entity> entities = frameLoc.getWorld().getNearbyEntities(Utils.centerByFace(frameLoc.getBlock().getLocation()), 0.5D,0.5D,0.5D);
						for (Entity entity : entities) {
							if (entity instanceof ItemFrame) {
								ItemFrame frame = (ItemFrame) entity;
								frame.setPersistent(true);
								ItemStack mapItem = new ItemStack(Material.FILLED_MAP);
								MapMeta meta = (MapMeta)mapItem.getItemMeta();
								meta.setMapId(mapId);
								mapItem.setItemMeta(meta);
								frame.setItem(mapItem);
							}
						}
					}
				}
			},30L);
		}
		
	}

}
