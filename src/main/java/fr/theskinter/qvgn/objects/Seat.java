package fr.theskinter.qvgn.objects;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.data.Bisected.Half;
import org.bukkit.block.data.type.Stairs;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

import fr.theskinter.qvgn.Game;
import fr.theskinter.qvgn.Nitro;
import fr.theskinter.qvgn.core.SpecsSkins;
import fr.theskinter.qvgn.core.Utils;
import lombok.Getter;
import lombok.Setter;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.npc.skin.SkinnableEntity;

public class Seat {

	@Getter @Setter private String standID;
	@Getter @Setter private String whosUsing;
	@Getter @Setter private boolean isGlowing = false;
	@Getter private boolean isUsed = false;
	@Getter @Setter private boolean isNPC = false;
	@Getter @Setter private World world;
	@Getter @Setter private Block block;
	@Getter @Setter private Location loc;
	@Getter @Setter private boolean forSpectator = true;
	
	public Seat(Block block) {
		setBlock(block);
		setLoc(block.getLocation().add(0.5D, 0, 0.5D));
		setWorld(block.getWorld());
		Stairs stair = (Stairs) block.getState().getBlockData();
		Location lookAtLoc = lookAt(loc, block.getRelative(stair.getFacing().getOppositeFace()).getLocation().add(0.5D,0,0.5D));
		ArmorStand stand = (ArmorStand)getWorld().spawnEntity(Utils.getRelative(loc, 0, -1, 0), EntityType.ARMOR_STAND);
		stand.setGravity(false); stand.setCustomNameVisible(false); stand.setVisible(false);
		Bukkit.getScheduler().runTaskLater(Nitro.instance,new Runnable() {
			@Override
			public void run() {
				stand.setRotation(lookAtLoc.getYaw(), lookAtLoc.getPitch());				
			}
		},2L);
		
		standID = stand.getUniqueId().toString();
	}
	
	public boolean setUsing(Entity entity) {
		if (isUsed) return false;
		if (standID == null) return false;
		if (Bukkit.getEntity(UUID.fromString(standID)) == null) return false;
		if (!Bukkit.getEntity(UUID.fromString(standID)).getPassengers().isEmpty()) return false;
		Bukkit.getEntity(UUID.fromString(standID)).addPassenger(entity);
		if (isNPC) { whosUsing = CitizensAPI.getNPCRegistry().getNPC(entity).getUniqueId().toString(); }
		else { whosUsing = entity.getUniqueId().toString(); }
		isUsed = true;
		return true;
	}
	
	public boolean eject() {
		if (!isUsed) return false;
		if (standID == null) return false;
		if (Bukkit.getEntity(UUID.fromString(standID)) == null) return false;
		if (Bukkit.getEntity(UUID.fromString(standID)).getPassengers().isEmpty()) return false;
		Entity passenger = Bukkit.getEntity(UUID.fromString(standID)).getPassengers().get(0);
		passenger.teleport(Utils.getRelative(passenger.getLocation(),0,1,0));
		Bukkit.getEntity(UUID.fromString(standID)).eject();
		isUsed = false; isNPC = false; whosUsing = null;
		return true;
	}
	
	public void manualEject(Entity entity) {
		if (!isUsed) return; if (isNPC) return;
		if (standID == null) return;
		if (UUID.fromString(whosUsing).equals(entity.getUniqueId())) {
			Location standUpLocation = Utils.getRelative(block.getLocation(),0.5D,1,0.5D);
			standUpLocation.setYaw(entity.getLocation().getYaw());
			standUpLocation.setPitch(entity.getLocation().getPitch());
			Bukkit.getScheduler().runTaskLater(Nitro.instance, new Runnable() {
				@Override
				public void run() {
					entity.teleport(standUpLocation);	
				} }, 2L);
			isUsed = false; whosUsing = null;
		}
	}
	
	public void fillWithNPC() {
		if (isUsed) return;
		int specIndex = Game.instance.getSpecIndex();
		NPC npc = CitizensAPI.getNPCRegistry().createNPC(EntityType.PLAYER, "["+specIndex+"]");
		Location spawnLoc = Utils.getRelative(loc,0, -2, 0);
		if (Game.instance.getSceneCenter() != null) {
			Location lookAtLoc = lookAt(spawnLoc, Game.instance.getSceneCenter().getBlock().getLocation().add(0.5D,0,0.5D));
			Bukkit.getScheduler().runTaskLater(Nitro.instance,new Runnable() {
				@Override
				public void run() {
					((ArmorStand)Bukkit.getEntity(UUID.fromString(standID))).setRotation(lookAtLoc.getYaw(), lookAtLoc.getPitch());
				} },2L);
			
			npc.spawn(lookAtLoc);
		} else {
			npc.spawn(spawnLoc);
		}
		if (Game.instance.getUsableSkins().isEmpty()) { Game.instance.setUsableSkins(Utils.arrayToList(SpecsSkins.values())); } 
		SpecsSkins skin = Utils.randomFromList(Game.instance.getUsableSkins());
		if (npc.getEntity() instanceof SkinnableEntity) {
			npc.data().setPersistent(NPC.PLAYER_SKIN_TEXTURE_PROPERTIES_METADATA, skin.getTexture());
			npc.data().setPersistent(NPC.PLAYER_SKIN_TEXTURE_PROPERTIES_SIGN_METADATA, skin.getSignature());
		}
		Game.instance.getUsableSkins().remove(skin);
		npc.data().setPersistent(NPC.NAMEPLATE_VISIBLE_METADATA, false);
		Bukkit.getScheduler().runTaskLater(Nitro.instance, new Runnable() {
			@Override 
			public void run() {
				if (Game.instance.getSceneCenter() != null) { npc.faceLocation(Game.instance.getSceneCenter()); }
			}
		},5L);
		if (setUsing(npc.getEntity())) { setNPC(true); }
	}
	
	public static boolean canBeSeat(Block block) {
		if (block.getState().getBlockData() instanceof Stairs) {
			Stairs stair = (Stairs) block.getState().getBlockData();
			if (stair.getHalf() == Half.BOTTOM) {
				return true;
			}
		}
		return false;
	}
	
	public void unRegister() {
		if (isUsed) {
			if (isNPC()) {
				if (CitizensAPI.getNPCRegistry().isNPC(Bukkit.getEntity(UUID.fromString(getWhosUsing())))) {
					NPC npc = CitizensAPI.getNPCRegistry().getNPC(Bukkit.getEntity(UUID.fromString(getWhosUsing())));
					eject(); npc.despawn(); npc.destroy();
				}
			} else { eject(); }
		}
		if (!Bukkit.getEntity(UUID.fromString(standID)).isDead()) Bukkit.getEntity(UUID.fromString(standID)).remove();
	}
	
    public Location lookAt(Location from,Location point) {
    	Location newHeadPos;
    	double x = from.getX(); double y = from.getY(); double z = from.getZ();
        double xDiff = point.getX() - from.getX();
        double yDiff = point.getY() - from.getY();
        double zDiff = point.getZ() - from.getZ();

        double DistanceXZ = Math.sqrt(xDiff * xDiff + zDiff * zDiff);
        double DistanceY = Math.sqrt(DistanceXZ * DistanceXZ + yDiff * yDiff);
        double newYaw = Math.acos(xDiff / DistanceXZ) * 180 / Math.PI;
        double newPitch = Math.acos(yDiff / DistanceY) * 180 / Math.PI - 90;
        if (zDiff < 0.0)
            newYaw = newYaw + Math.abs(180 - newYaw) * 2;
        newYaw = (newYaw - 90);
        newHeadPos = new Location(from.getWorld(),x, y, z,(float)newYaw,(float)newPitch);
        return newHeadPos;
    }
    
}
