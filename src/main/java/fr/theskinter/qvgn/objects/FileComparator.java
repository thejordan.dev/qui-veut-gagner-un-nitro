package fr.theskinter.qvgn.objects;

import java.io.File;
import java.util.Comparator;

public class FileComparator implements Comparator<File> {

	@Override
	public int compare(File o1, File o2) {
		if (o1.getName().contains("question") && o2.getName().contains("question")) {
			String o1Name = o1.getName().replaceAll("question", ""); String o2Name = o2.getName().replaceAll("question", "");
			int numo1 = Integer.parseInt(o1Name); int numo2 = Integer.parseInt(o2Name);
			return Integer.compare(numo1, numo2);
		}
		return o1.getName().compareTo(o2.getName());
	}

}
