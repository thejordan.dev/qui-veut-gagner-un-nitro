package fr.theskinter.qvgn.commands;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.map.MapView;

import fr.theskinter.qvgn.Game;
import fr.theskinter.qvgn.Game.RegisterType;
import fr.theskinter.qvgn.core.Utils;
import fr.theskinter.qvgn.Nitro;
import fr.theskinter.qvgn.managers.Menus;

public class CMD_Main extends CMD {

	public CMD_Main() {
		super("nitro");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player)sender;
			if (!player.hasPermission("nitro.control")) return false;
			if (args.length == 1) {
				if (args[0].equalsIgnoreCase("chooseQuestion")) {
					Menus.instance.getMenuQuestions().open(player);
				} else if (args[0].equalsIgnoreCase("setCenter")) {
					Game.instance.setSceneCenter(player.getLocation().getBlock().getLocation().add(0.5D, 0.5D, 0.5D));
					player.sendMessage("�a�lCentre de s�ne d�finie !");
				} else if (args[0].equalsIgnoreCase("registerSeats")) {
					if (!Game.instance.getPlayersStates().containsKey(player.getUniqueId())) {
						Game.instance.getPlayersStates().put(player.getUniqueId(), RegisterType.SEAT);
						player.sendMessage("�a�lCliquez droit pour enregistrer les sieges");
					} else if (Game.instance.getPlayersStates().get(player.getUniqueId()) == RegisterType.SEAT) {
						Game.instance.getPlayersStates().put(player.getUniqueId(), RegisterType.NONE);
					}
				} else if (args[0].equalsIgnoreCase("setupMaps")) {
					if (Game.instance.getMapsIds().isEmpty()) {
						List<Integer> ids = new ArrayList<>();
						for (int i = 0;i<66;i++) {
							MapView view = Bukkit.createMap(player.getWorld());
							ids.add(view.getId());
						}
						Game.instance.getMapsIds().addAll(ids);
						player.sendMessage(ids.size() + " maps r�serv�es : ["+ids.get(0)+" ... "+ids.get(ids.size()-1)+"]");
					} else {
						List<Integer> ids = Game.instance.getMapsIds();
						player.sendMessage(ids.size() + " maps d�j� r�serv�es : ["+ids.get(0)+" ... "+ids.get(ids.size()-1)+"]");
					}
				} else if (args[0].equalsIgnoreCase("definScreen")) {
					Game.instance.getPlayersStates().put(player.getUniqueId(), RegisterType.bTOP_LEFT);
					player.sendMessage("�a�lVeuillez s�lectionner le coin sup�rieur gauche !");
				} else if (args[0].equalsIgnoreCase("createBoard")) {
					if (Game.instance.getScreen().getBorder().getTopLeftLoc() != null && Game.instance.getScreen().getBorder().getBotRightLoc() != null && Game.instance.getScreen().getFacing() != null) {
						BlockFace facing = Game.instance.getScreen().getFacing();
						Location topLeft = Game.instance.getScreen().getBorder().getTopLeftLoc();
						Location botRight = Game.instance.getScreen().getBorder().getBotRightLoc();
						
						Location backTopLeftCorner = topLeft.getBlock().getRelative(facing).getLocation();
						Location backBotRIghtCorner = botRight.getBlock().getRelative(facing).getLocation();
						Utils.fill(backTopLeftCorner,backBotRIghtCorner,Material.POLISHED_ANDESITE, true);
						List<Location> locsG = Game.instance.getScreen().loopLocationsInBetween();
						for (Location location : locsG) {
							Collection<Entity> entities = location.getWorld().getNearbyEntities(Utils.centerByFace(location.getBlock().getLocation()), 0.7D,0.7D,0.7D);
							for (Entity entity : entities) {
								if (entity instanceof ItemFrame) {
									entity.remove();
								}
							}
						}
						Bukkit.getScheduler().runTaskLater(Nitro.instance, new Runnable() {
							@Override
							public void run() {
								for (Location location : locsG) {
									ItemFrame frame = (ItemFrame) location.getWorld().spawnEntity(location, EntityType.ITEM_FRAME);
									frame.setFacingDirection(facing);
								}
							}
						}, 5L);

						//SchedulerManager.instance.setupFrame(locsG, facing); SchedulerManager.instance.launchItemFrame();
					}
				}
			}
		}
		return false;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		List<String> returned = new ArrayList<String>();
		if (!sender.hasPermission("nitro.control")) return returned;
		if (args.length == 1) {
			returned.add("chooseQuestion");
			returned.add("setupMaps");
			returned.add("definScreen");
		}
		return returned;
	}

}
