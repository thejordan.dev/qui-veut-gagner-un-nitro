package fr.theskinter.qvgn.commands;

import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import fr.theskinter.qvgn.managers.Commands;

public abstract class CMD implements CommandExecutor,TabCompleter {

	private String cmdName;
	
	public CMD(String cmdName) { this.cmdName = cmdName; register(); }
	
	private void register() { Commands.instance.defineCommand(cmdName, this); }
	
	@Override public abstract List<String> onTabComplete(CommandSender sender,Command cmd, String label, String[] args);
	@Override public abstract boolean onCommand(CommandSender sender, Command cmd, String label, String[] args);

}