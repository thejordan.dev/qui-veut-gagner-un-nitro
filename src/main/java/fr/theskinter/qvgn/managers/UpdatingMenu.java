package fr.theskinter.qvgn.managers;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import fr.theskinter.qvgn.Nitro;
import lombok.Getter;

public abstract class UpdatingMenu extends Menu {

	@Getter private Integer taskID;
	
	@SuppressWarnings("deprecation")
	public UpdatingMenu(String id, String name, int lines) {
		super(id, name, lines);
		Menus.instance.getUpdatables().add(this);
		this.taskID = Bukkit.getScheduler().scheduleAsyncRepeatingTask(Nitro.instance, new BukkitRunnable() {
			
			@Override
			public void run() {
				if (getInventory().getViewers().size() > 0) {
					update();
				}
			}
		}, 0L, 20L);
		
	}
	
	private void update() {
		setup();
	}

}
