package fr.theskinter.qvgn.managers;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import fr.theskinter.qvgn.core.Utils;


public class MenuItems {

	public static ItemStack questionItem(File file) {
		String fileName = file.getName();
		ItemStack item = new ItemStack(Material.BOOK);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName("�r�l"+fileName);
		item.setItemMeta(meta);
		return item;
	}
	
	public static ItemStack imageItem(File file) {
		BufferedImage image = Utils.fromFile(file);
		String fileName = file.getName();
		ItemStack item = new ItemStack(Material.MAP);
		ItemMeta meta = item.getItemMeta();
		String name = fileName.replaceAll(".png", "");
		meta.setDisplayName("�r�l"+name);
		meta.setLore(Arrays.asList("�e�l"+image.getWidth()+"x"+image.getHeight()));
		item.setItemMeta(meta);
		return item;
	}
	
	public static ItemStack refreshItem() {
		ItemStack item = new ItemStack(Material.TOTEM_OF_UNDYING);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName("�a�lREFRESH");
		item.setItemMeta(meta);
		return item;
	}
	
	public static ItemStack backItem() {
		ItemStack item = new ItemStack(Material.BARRIER);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName("�c�lRETOUR");
		item.setItemMeta(meta);
		return item;
	}
	
}
