package fr.theskinter.qvgn.managers;

import org.bukkit.plugin.java.JavaPlugin;

import fr.theskinter.qvgn.listeners.EventListener;
import fr.theskinter.qvgn.listeners.PlayerEventListener;
import lombok.Getter;

public class Events {

	public static Events instance;
	@Getter private JavaPlugin plugin;
	
	public Events(JavaPlugin plugin) {
		instance = this;
		this.plugin = plugin;
		new PlayerEventListener(plugin);
	}
	
	public void registerEvents(EventListener listener) {
		plugin.getServer().getPluginManager().registerEvents(listener, plugin);
	}

	public static void init(JavaPlugin plugin) {
		if (instance != null) return;
		new Events(plugin);
	}
	
}
