package fr.theskinter.qvgn.managers;

import org.bukkit.plugin.java.JavaPlugin;

import fr.theskinter.qvgn.commands.CMD;
import fr.theskinter.qvgn.commands.CMD_Main;

public class Commands {

	public static Commands instance;
	
	private JavaPlugin plugin;
	
	public Commands(JavaPlugin plugin) {
		instance = this;
		this.plugin = plugin;
		new CMD_Main();
	}
	
	public static void init(JavaPlugin plugin) {
		if (instance != null) return;
		new Commands(plugin);
	}
	
	public void defineCommand(String name,CMD cmd) {
		plugin.getCommand(name).setExecutor(cmd);
		plugin.getCommand(name).setTabCompleter(cmd);
	}
	
}
