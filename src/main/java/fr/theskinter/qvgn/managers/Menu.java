package fr.theskinter.qvgn.managers;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

import lombok.Getter;

public abstract class Menu implements InventoryHolder {

	@Getter private final String id;
	private final Inventory inventory;
	@Override public Inventory getInventory() { return this.inventory; }
	
	public Menu(String id,String name,int lines) {
		this.id = id;
		this.inventory = Bukkit.createInventory(this, lines*9, name);
		Menus.instance.getMenus().add(this);
	}
	
	public void setBackground(ItemStack item) {
		for (int i = 0;i<inventory.getSize();i++) {
			this.inventory.setItem(i, item);
		}
	}
	
	public void setSlot(ItemStack item,int slot) {
		this.inventory.setItem(slot, item);
	}
	
	public void open(Player player) {
		setup();
		player.openInventory(inventory);
	}
	
	public abstract void setup();
	
	public abstract void onClick(Player who,ItemStack item,ClickType click,boolean shift,boolean left,boolean right,InventoryClickEvent event);

	public abstract void onClose(Player who,InventoryCloseEvent event);

}
