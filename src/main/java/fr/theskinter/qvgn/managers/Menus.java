package fr.theskinter.qvgn.managers;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.imageio.ImageIO;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

import fr.theskinter.qvgn.Game;
import fr.theskinter.qvgn.Nitro;
import fr.theskinter.qvgn.core.Utils;
import fr.theskinter.qvgn.objects.FileComparator;
import lombok.Getter;

public class Menus {

	@Getter public static Menus instance;
	@Getter private final List<Menu> menus = new ArrayList<Menu>();
	@Getter private final List<UpdatingMenu> updatables = new ArrayList<UpdatingMenu>();

	@Getter private final MENU_Questions menuQuestions;
	
	public static void init() {
		if (instance != null) return;
		instance = new Menus();
	}
	
	public Menus() {
		instance = this;
		this.menuQuestions = new MENU_Questions();
	}
	
	public static MENU_Images createImagesMenu(String name) {
		MENU_Images menu = new MENU_Images(name);
		Menus.instance.getMenus().add(menu);
		return menu;
	}
	
	public static MENU_Images getQuestionByNum(String name) {
		String ID = MenusIDs.IMAGES+"."+name;
		MENU_Images menu;
		if (Menus.instance.doesMenuExist(ID)) { menu = (MENU_Images) Menus.instance.getMenu(ID); }
		else { menu = createImagesMenu(name); }
		return menu;
	}
	
	public boolean doesMenuExist(InventoryHolder holder) {
		for (Menu menu : menus) { if (holder == menu) { return true; } } return false;
	}
	
	public Menu getMenu(InventoryHolder holder) {
		for (Menu menu : menus) { if (holder == menu) { return menu; } } return null;
	}
	
	public boolean doesMenuExist(String id) {
		for (Menu menu : menus) { if (menu.getId().equals(id)) { return true; } } return false;
	}
	public Menu getMenu(String id) {
		for (Menu menu : menus) { if (menu.getId().equals(id)) { return menu; } } return null;
	}
	
	public class MenusIDs {
		static final String QUESTIONS = "nitro.questions";
		static final String IMAGES = "nitro.images";
	}
	
	public class MENU_Questions extends Menu {

		public MENU_Questions() {
			super(MenusIDs.QUESTIONS, "Nitro - Questions", 4);
		}

		@Override
		public void open(Player player) {
			super.open(player);
		}
		
		@Override
		public void setup() {
			setBackground(new ItemStack(Material.AIR));
			File mainDir = new File(Nitro.instance.getDataFolder(),"images");
			List<File> folders = Utils.getQuestionFolders(mainDir.getPath());
			Collections.sort(folders, new FileComparator());
			List<File> others = Utils.getOthersFolders(mainDir.getPath());
			Collections.sort(others, new FileComparator());
			folders.addAll(others);
			for (int i = 0;i<folders.size();i++) {
				setSlot(MenuItems.questionItem(folders.get(i)), i);
			}
			setSlot(MenuItems.refreshItem(), 31);
		}

		@Override
		public void onClick(Player who, ItemStack item, ClickType click, boolean shift, boolean left, boolean right, InventoryClickEvent event) {
			event.setCancelled(true);
			if (event.getSlotType() == SlotType.OUTSIDE) return;
			if (item == null) return;
			if (item.isSimilar(MenuItems.refreshItem())) {
				setup();
			} else if (item.getType() == Material.BOOK) {
				String itemName = ChatColor.stripColor(item.getItemMeta().getDisplayName());
				File file = new File(Nitro.instance.getDataFolder(),"images/"+itemName);
				if (file.exists()) {
					Menus.getQuestionByNum(file.getName()).open(who);
				}
				setup();
			}
		}

		@Override
		public void onClose(Player who, InventoryCloseEvent event) {
			
		}
		
	}
	
	public static class MENU_Images extends Menu {

		@Getter private String name;
		@Getter private File dir;
		
		public MENU_Images(String name) {
			super(MenusIDs.IMAGES+"."+name, "Nitro - Images", 4);
			this.name = name;
			this.dir = new File(Nitro.instance.getDataFolder(),"images/"+name);
		}

		@Override
		public void open(Player player) {
			super.open(player);
		}
		
		@Override
		public void setup() {
			setBackground(new ItemStack(Material.AIR));
			for (int i = 0;i<Utils.getImagesFrom(this.dir.getPath()).size();i++) {
				setSlot(MenuItems.imageItem(Utils.getImagesFrom(dir.getPath()).get(i)), i);
			}
			setSlot(MenuItems.backItem(), 27);
			setSlot(MenuItems.refreshItem(), 31);
		}

		@Override
		public void onClick(Player who, ItemStack item, ClickType click, boolean shift, boolean left, boolean right, InventoryClickEvent event) {
			event.setCancelled(true);
			if (event.getSlotType() == SlotType.OUTSIDE) return;
			if (item == null) return;
			if (item.isSimilar(MenuItems.refreshItem())) {
				setup();
			} else if (item.isSimilar(MenuItems.backItem())) {
				Menus.instance.getMenuQuestions().open(who);
			} else if (item.getType() == Material.MAP) {
				File file = Utils.getImagesFrom(dir.getPath()).get(event.getRawSlot());
				BufferedImage image = null;
				try { if (ImageIO.read(file) == null) return; image = ImageIO.read(file); }
				catch (IOException e) { e.printStackTrace(); }
				if (image == null) return;
				if (Game.instance.getScreen().isSetup()) {
					Game.instance.getScreen().setCurrentImage(file);
					Game.instance.getScreen().drawImage(image);
				}
				setup();
			}
		}

		@Override
		public void onClose(Player who, InventoryCloseEvent event) {
			if (getInventory().getViewers().size() == 0) {
				Menus.instance.getMenus().remove(this);
			}
		}
	}
}
